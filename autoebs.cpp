/* This requires C++17 or above, e.g. g++ -std=c++17 -o autoebs autoebs.cpp */
#include <algorithm>
#include <cassert>
#include <cinttypes>
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <functional>
#include <iostream>
#include <optional>
#include <string_view>
#include <vector>

using uint128_t = __uint128_t;

struct WindowDatum {
    uint64_t blockHeight{};
    uint64_t blockSize{};
    uint64_t excessiveBlockSize{};
};

using WindowData = std::vector<WindowDatum>;

struct AEBS {
    uint32_t growthFactorNumerator{};  // Note: {} syntax auto-inits to '0'
    uint32_t growthFactorDenominator{};
    uint8_t headroomFactor{};
    uint64_t activationHeight{};

    uint64_t calculateAutoExcessiveBlocksize(uint64_t previousBlockSize, uint64_t previousExcessiveBlocksize) const;
};

uint64_t AEBS::calculateAutoExcessiveBlocksize(uint64_t previousBlockSize, uint64_t previousExcessiveBlocksize) const
{
    if (previousBlockSize * uint64_t(this->headroomFactor) > previousExcessiveBlocksize) {
        assert(this->growthFactorDenominator != 0); /* prevent div-by-0 */
        uint64_t excessiveBlockSize = static_cast<uint64_t>(
            static_cast<uint128_t>(previousExcessiveBlocksize) * static_cast<uint128_t>(this->growthFactorNumerator)
            / static_cast<uint128_t>(this->growthFactorDenominator)
        );
        // Required for really small configAutoExcessiveBlocksize where
        // calculated increase would be less than 1 byte and get truncated
        if (excessiveBlockSize == previousExcessiveBlocksize)
            ++excessiveBlockSize;
        return excessiveBlockSize;
    }
    return previousExcessiveBlocksize;
}

static bool getWindowData(WindowData &windowData) {
    for (auto & w : windowData) {
        if (std::scanf("%" PRIu64 ",%" PRIu64, &w.blockHeight, &w.blockSize) != 2)
            return false;
    }
    return true;
}

static void calculateWindowExcessiveBlocksizes(WindowData &windowData, uint64_t previousBlockSize,
                                               uint64_t previousExcessiveBlocksize,
                                               uint64_t configExcessiveBlocksize, const std::optional<AEBS> &optAebs)
{
    assert(!windowData.empty());
    if (!optAebs || optAebs->activationHeight > windowData[0].blockHeight)
        windowData[0].excessiveBlockSize = configExcessiveBlocksize;
    else
        windowData[0].excessiveBlockSize = optAebs->calculateAutoExcessiveBlocksize(previousBlockSize,
                                                                                    previousExcessiveBlocksize);
    for (size_t i = 1; i < windowData.size(); ++i) {
        auto & w = windowData[i], & wPrev = windowData[i-1];
        if (!optAebs || optAebs->activationHeight > w.blockHeight)
            w.excessiveBlockSize = configExcessiveBlocksize;
        else
            w.excessiveBlockSize = optAebs->calculateAutoExcessiveBlocksize(wPrev.blockSize, wPrev.excessiveBlockSize);
    }
}

static void printWindow(const WindowData &windowData) {
    for (const auto & w : windowData)
        std::printf("%" PRIu64 ",%" PRIu64 ",%" PRIu64 "\n",
                    w.blockHeight, w.blockSize, w.excessiveBlockSize);
}

static void printWindowAggregates (const WindowData &windowData) {
    std::vector<uint64_t> sortedBlockSizes(windowData.size());
    uint64_t minBlockSize = windowData[0].blockSize;
    uint64_t maxBlockSize = minBlockSize;
    for (size_t i = 0; i < windowData.size(); ++i) {
        const auto bs = windowData[i].blockSize;
        minBlockSize = std::min(bs, minBlockSize);
        maxBlockSize = std::max(bs, maxBlockSize);
        sortedBlockSizes[i] = bs; // populate for sort below
    }
    std::sort(sortedBlockSizes.begin(), sortedBlockSizes.end());
    assert(!windowData.empty());
    assert(windowData.size() > 0);
    uint64_t medianBlockSize;
    if (windowData.size() % 2 == 0)
        medianBlockSize = (sortedBlockSizes[windowData.size() / 2u - 1] +
                           sortedBlockSizes[windowData.size() / 2u]) / 2u;
    else
        medianBlockSize = sortedBlockSizes[windowData.size() / 2u];
    std::printf("%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 "\n",
                windowData[0].blockHeight, windowData.back().blockHeight,
                minBlockSize, medianBlockSize, maxBlockSize,
                windowData[0].excessiveBlockSize, windowData.back().excessiveBlockSize);
}

// Usage: ./autoebs <-excessiveblocksize 1000000> [-autoebstart 79400] [-printaggregates]
int main (int argc, char *argv[])
{
    // Process arguments
    uint64_t configExcessiveBlocksize{};
    std::optional<AEBS> optAebs;;
    bool configPrintAggregates = false;
    if (argc < 3) {
        std::cerr << "Please pass args\n";
        return EXIT_FAILURE;
    }
    if (std::string_view{argv[1]} == "-excessiveblocksize") {
        if (std::sscanf(argv[2], "%" PRIu64, &configExcessiveBlocksize) != 1)
            return EXIT_FAILURE;
        if (configExcessiveBlocksize < 285)
            return EXIT_FAILURE;
    }
    else
        return EXIT_FAILURE;
    if (argc > 3 && std::string_view{argv[3]} == "-autoebstart") {
        uint64_t configAEBActivationHeight;
        if (std::sscanf(argv[4], "%" PRIu64, &configAEBActivationHeight) != 1)
            return EXIT_FAILURE;
        optAebs.emplace();
        optAebs->headroomFactor = 8;
        optAebs->growthFactorNumerator = 4294967295u;
        optAebs->growthFactorDenominator = 4294910693u;
        optAebs->activationHeight = configAEBActivationHeight;
    }
    if (argc > 3 && std::string_view{argv[argc-1]} == "-printaggregates") {
        configPrintAggregates = true;
    }

    // Initialize cache
    constexpr size_t BLOCKCHAIN_DAY = 144;
    WindowData windowData(BLOCKCHAIN_DAY);  // init vector to size BLOCKCHAIN_DAY

    // Print config used
    std::cout << "; -excessiveblocksize " << configExcessiveBlocksize;
    if (optAebs) std::cout << " -autoebstart " << optAebs->activationHeight;
    std::cout << "\n";

    // Print row headers
    if (configPrintAggregates)
        std::cout << "openHeight,closeHeight,minBlockSize,medianBlockSize,maxBlocksize,openExcessiveBlocksize"
                     ",closeExcessiveBlocksize\n";
    else
        std::cout << "blockHeight,blockSize,excessiveBlocksize\n";

    // Calculate and print
    uint64_t previousBlockSize = 0;
    uint64_t previousExcessiveBlockSize = configExcessiveBlocksize;
    auto * const printFunc = configPrintAggregates ? printWindowAggregates : printWindow;
    while (getWindowData(windowData)) {
        calculateWindowExcessiveBlocksizes(windowData, previousBlockSize, previousExcessiveBlockSize,
                                           configExcessiveBlocksize, optAebs);
        printFunc(windowData); // either printWindowAggregates() or printWindow()
        previousBlockSize = windowData.back().blockSize;
        previousExcessiveBlockSize = windowData.back().excessiveBlockSize;
    }

    return EXIT_SUCCESS;
}
